PROJECT SPEC

메디스태프 앱 개발 : (2019.09~현재)  

클라이언트 : 메디스태프 B2C App
안드로이드 앱 개발 및 안정화 수행
API 일부 공동 개발
의사들을 위한 보안메신저 플랫폼

블로그 링크 :  https://blog.naver.com/skyms2000/221900523270
플레이스토어 링크 : https://play.google.com/store/apps/details?id=com.org.medistaffapp
홈페이지 : http://www.medistaff.co.kr/



축산차량통합 관제 현장 관리 앱 개발 :  (2018.11~2019.04) 

클라이언트 : 농림축산검역본부(김천) 사업의 일부. 
안드로이드 앱 개발 및 안정화 수행
API 일부 공동 개발
역학조사를 수행하는 현장요원들의 업무지원 앱

블로그 링크 :  https://blog.naver.com/skyms2000/221470792108


HoneyCube : (2017.11~2018.07) 

아기의 건강상태를 모니터링하는 헬스케어 디바이스 인 허니큐브 앱
안드로이드 앱 개발 및 유지보수
관리자페이지 유지보수 

앱 링크 : https://play.google.com/store/apps/details?id=honeynaps.cube.app



WANPANGO  : (2017.11~2018.07)  

부동산 분양 서비스 및 구인구직 시스템 
안드로이드 개발 (웹앱형태로 개발)
웹 관리자 페이지 및 FCM 시스템 구현

블로그 링크 :  https://blog.naver.com/skyms2000/221470792108
홈페이지 주소 : http://wanpango.com



KWAVE U : (2016.11~2017.08)

연애인 소원 프로젝트.

안드로이드 1인 개발 
API 일부 공동 개발
iOS 유지보수 
App 중국 마켓 진출(웨이보,QQ등)

블로그 링크 :  https://blog.naver.com/skyms2000/220920831907



증평 쇼핑몰  : (2016.11~2017.06)  

증평 명품 인삼 쇼핑몰
LAMP Stack Setting.
관리자 페이지 유지 보수 

http://jpginsengmall.co.kr/

 

드라마켓  (2016.11~2017.06) 

드라마에서 나온 연애인들의 의상 비교 사이트  
LAMP Stack Setting.
관리자 페이지 유지 보수 

홈페이지 링크 : http://dramarket.co.kr/



캔딧 포토앨범북  : (2015.03~2015.11) 

포토 앨범(포토북 제작 및 상품 구매)
안드로이드 1인 개발 및 기획 참가 
API 일부 공동 개발

블로그 링크 :  https://blog.naver.com/skyms2000/220663876119

 


유니챌 모바일 홈페이지 (2014.12~2015.01) 

회사 모바일 홈페이지
Smart TV (Enyo)를 이용한 간단한 게임 앱 
HTML5 , JQuery Mobile 

블로그 링크 :  https://blog.naver.com/skyms2000/220437340402

 


LG 모바일 연구소 모바일 App팀 (2011.12~ 2014.04)

LG폰 각종 모델 에 들어가는  Native App개발.
LG Setting App : 2011.12 ~ 2012.09
LG PC Sync  : 2013.03 ~ 2014.04

 
인사동 갤러리 프로젝트(2011.09~2011.11) 

인사동 갤러리를 소개해주는 컨텐츠 앱. 
안드로이드 1인 개발, 윈도우 모바일 앱 공동 개발 
안드로이드 SDK 2.1

블로그 링크 : https://blog.naver.com/skyms2000/220123621566.



졸업 작품 프로젝트 : 가제 - 여행 앨범 다이어리 (2010.09~2010.11) 

여행장소를 기반으로  다이어리를 작성하는 컨텐츠 앱. 
안드로이드 1인 개발 
안드로이드 SDK 2.1

블로그 링크 :  https://blog.naver.com/skyms2000/10102073370


학과 프로젝트 (2009 ~ 2010) 

유닉스 System Call과 Socket 및 멀티프로세스를  활용한 Text Mud Game 개발. 
자바의 소켓과 멀티 프로세스를 활용한 본교를 배경으로 한 한 부르마블 게임 제작(가제 : SNUT 마블) 
ASP.NET을 이용한 영화 예매 웹 어플리케이션 구현.

블로그 링크 :  https://blog.naver.com/skyms2000/220130290793


















**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).